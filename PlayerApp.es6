import {Component, Template, bootstrap,Foreach} from 'angular2/angular2';
import {PlayerStore} from 'services/PlayerStore';

    @Component({
      selector: 'player-app',
      componentServices: [PlayerStore]
    })

    @Template({
      url: 'templates/player.html',
      directives: [Foreach]
    })

    class PlayerApp {

      playerStore : PlayerStore;

        constructor(playerStore: PlayerStore) {
            this.playerStore = playerStore;
        }

        add(newplayer,newleague,newteam){
            this.playerStore.add(newplayer.value, newleague.value, newteam.value);
            newplayer.value = '';
        }

        remove(player) {
           this.playerStore.remove(player.name,player.team, player.league);
        }

        toggleYellow(player){
            player.yellow = !player.yellow;
        }

        toggleRed(player){
            player.red =! player.red;
        }

    }

    bootstrap(PlayerApp);