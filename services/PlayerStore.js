export class PlayerStore {
    
    constructor(){
        this.playerList = [];
    }
    add(name,league,team){
        this.playerList.unshift({name:name, league:league, team:team, done:false});
    }
    
    remove(name,team,league){
        for(var i=0;i<this.playerList.length;i++){
           if(this.playerList[i].name === name && this.playerList[i].team === team && this.playerList[i].league) {
            this.playerList.splice(i,1);
            break;
            }
        }
    }
    
}